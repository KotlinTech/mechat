package drivebygigs.tk.fragment


import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import drivebygigs.tk.CommonMethods

import drivebygigs.tk.R
import drivebygigs.tk.adapter.UsersAdapter
import kotlinx.android.synthetic.main.activity_create_account.*
import kotlinx.android.synthetic.main.fragment_user.*


class UserFragment : Fragment() {
    var mUserDatabase: DatabaseReference? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        if (CommonMethods().isConnectingToInternet(context!!)) {
            mUserDatabase = FirebaseDatabase.getInstance().reference.child("Users")
        }else{
            val snackbar = Snackbar
                .make(relativeLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG)
            CommonMethods().showSnackbar(snackbar,30,30,context!!)
        }

        userRecyclerViewId.setHasFixedSize(true)

        userRecyclerViewId.layoutManager = linearLayoutManager
        userRecyclerViewId.adapter = UsersAdapter(mUserDatabase!!, context!!)
    }

}
