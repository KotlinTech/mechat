package drivebygigs.tk

import android.content.Context
import android.support.design.widget.Snackbar
import android.widget.FrameLayout
import android.net.NetworkInfo
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.res.Resources
import android.support.v4.content.ContextCompat.getSystemService
import android.net.ConnectivityManager
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.getColor


class CommonMethods {

    fun showSnackbar(snackbar: Snackbar, sideMargin: Int, marginBottom: Int,context: Context) {
        val snackBarView = snackbar.view
        // Depend upon your parent Layout Use `LayoutParams` of that Layout
        val params = snackBarView.layoutParams as FrameLayout.LayoutParams

        params.setMargins(
            params.leftMargin + sideMargin,
            params.topMargin,
            params.rightMargin + sideMargin,
            params.bottomMargin + marginBottom
        )

        snackBarView.layoutParams = params
        snackBarView.setBackgroundColor(getColor(context,android.R.color.white));
        snackbar.setActionTextColor(getColor(context,R.color.colorPrimaryDark));

        snackbar.show()
    }

    fun isConnectingToInternet(context: Context): Boolean {
        val connectivity = context.getSystemService(
            Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivity.allNetworkInfo
        if (info != null)
            for (i in info)
                if (i.state == NetworkInfo.State.CONNECTED) {
                    return true
                }
        return false
    }
}