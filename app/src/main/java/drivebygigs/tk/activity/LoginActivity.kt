package drivebygigs.tk.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.TextUtils
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import drivebygigs.tk.CommonMethods
import drivebygigs.tk.R
import kotlinx.android.synthetic.main.activity_create_account.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    var mAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()
        loginButtonId.setOnClickListener {
            if (CommonMethods().isConnectingToInternet(this)) {
                val email = loginEmailE.text.toString().trim()
                val password = loginPasswordEt.text.toString().trim()
                if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {
                    loginUser(email, password)
                } else {
                    val snackbar = Snackbar
                        .make(relativeLayout, "Login Failed!!!", Snackbar.LENGTH_LONG)
                    CommonMethods().showSnackbar(snackbar,30,30,this)
                }
            }else{
                val snackbar = Snackbar
                    .make(relativeLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG)
                CommonMethods().showSnackbar(snackbar,30,30,this)
            }
        }
    }

    private fun loginUser(email: String, password: String) {

        mAuth!!.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    val username = email.split("@")[0]
                    val dashboardIntent = Intent(this, DashBoardActivity::class.java)
                    dashboardIntent.putExtra("name", username)
                    startActivity(dashboardIntent)
                    finish()
                } else {
                    val snackbar = Snackbar
                        .make(relativeLayout, "Login Failed", Snackbar.LENGTH_LONG)
                    CommonMethods().showSnackbar(snackbar,30,30,this)
                }
            }
    }
}
