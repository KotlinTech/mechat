package drivebygigs.tk.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_create_account.*
import android.support.design.widget.Snackbar
import android.R
import android.widget.FrameLayout
import drivebygigs.tk.CommonMethods


class CreateAccountActivity : AppCompatActivity() {

    var mAuth: FirebaseAuth? = null
    var mDatabase: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(drivebygigs.tk.R.layout.activity_create_account)

        mAuth = FirebaseAuth.getInstance()

        accountCreateActBtn.setOnClickListener {
            if (CommonMethods().isConnectingToInternet(this)) {
                val email = accountEmailEt.text.toString().trim()
                val password = accountPasswordEt.text.toString().trim()
                val displayName = accountDisplayNameEt.text.toString().trim()

                if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)
                    || !TextUtils.isEmpty(displayName)
                ) {
                    createAccount(email, password, displayName)
                } else {
                    val snackbar = Snackbar
                        .make(relativeLayout, "Please fill out the fields", Snackbar.LENGTH_LONG)
                    CommonMethods().showSnackbar(snackbar,30,30,this)
                }
            } else {
                val snackbar = Snackbar
                    .make(relativeLayout, getString(drivebygigs.tk.R.string.no_internet_connection), Snackbar.LENGTH_LONG)
                CommonMethods().showSnackbar(snackbar,30,30,this)
            }

        }

    }


    private fun createAccount(email: String, password: String, displayName: String) {
        mAuth!!.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful){
                    val currentUser = mAuth!!.currentUser
                    val userId = currentUser!!.uid

                    mDatabase = FirebaseDatabase.getInstance().reference
                        .child("Users").child(userId)

                    val userObject = HashMap<String, String>()
                    userObject.put("display_name", displayName)
                    userObject.put("status", "Hello there....")
                    userObject.put("image", "default")
                    userObject.put("thumb_image", "default")

                    mDatabase!!.setValue(userObject).addOnCompleteListener {
                            task: Task<Void> ->
                        if (task.isSuccessful) {
                            val dashboardIntent = Intent(this, DashBoardActivity::class.java)
                            dashboardIntent.putExtra("name", displayName)
                            startActivity(dashboardIntent)
                            finish()
                        }else {
                            val snackbar = Snackbar
                                .make(relativeLayout, "User Not Created!!", Snackbar.LENGTH_LONG)
                            CommonMethods().showSnackbar(snackbar,30,30,this)
                        }
                    }
                }
            }
    }
}
